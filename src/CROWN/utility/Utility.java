package CROWN.utility;

import CROWN.Base.TestBase;
import com.github.javafaker.Faker;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static CROWN.utility.Randomstuff.ListRandom;
import static CROWN.utility.ScreenShot.screenShot;

public class Utility extends TestBase {


    public static boolean issAlertPresent() {
        try {
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    public static void AcceptAlert() {
        WebDriver drv = getdriver.get();
        Wait<WebDriver> wait = new WebDriverWait(drv, 10, 2);
        try {
            wait.until((Function<WebDriver, Object>) dr -> issAlertPresent());
            drv.switchTo().alert().accept();
            System.out.println("Alert was Present");
            TimeUnit.SECONDS.sleep(2);

        } catch (Exception e) {
            System.out.println("Alert was Absent");
        }
    }

    public static void CancelAlert() {
        WebDriver drv = getdriver.get();
        Wait<WebDriver> wait = new WebDriverWait(drv, 10, 2);
        try {
            wait.until((Function<WebDriver, Object>) dr -> isAlertPresent());
            drv.switchTo().alert().dismiss();
            System.out.println("Alert was Present");
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
            System.out.println("Alert was Absent");
        }
    }



    private static final String DATA_CONFIG = "./src/test/resources/config.properties";
    private static Properties properties;

    public static String getValue(String value) {
        try {
            if (properties == null) {
                Utility.properties = new Properties();
                Utility.properties.load(new FileInputStream(DATA_CONFIG));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Utility.properties.getProperty(value);
    }
    public static Object fetchService(String key) throws IOException {

        FileInputStream file = new FileInputStream("./Config/Service.properties");
        Properties property = new Properties();
        property.load(file);
        return property.get(key);
    }

    public static Object fetchProperty(String key) throws IOException {

        FileInputStream file = new FileInputStream("./Config/config.properties");
        Properties property = new Properties();
        property.load(file);
        return property.get(key);
    }

    public static String fetchLocator(String key) throws IOException {

        FileInputStream file = new FileInputStream("./Config/CicodLocators.properties");
        Properties property = new Properties();
        property.load(file);
        return property.get(key).toString();
    }

    //**********************Alert ********************
    public static void DowaitandAcceptAlerwhenReady(int timeOut) throws InterruptedException {
        Thread.sleep(1500);
        try {
            getDriver().switchTo().alert().accept();
            test.get().pass( "Alert Is Present During This Test");
        } catch (NoAlertPresentException e) {
            System.out.println("Alert is not Present");
            test.get().fail("Alert Not Present During this Test");
        }
    }

    static boolean acceptNextAlert = isAlertPresent();

    public static boolean isAlertPresent() {
        try {
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    public static void ScreenshotAlert() throws IOException, InterruptedException {
        if (acceptNextAlert) {
            screenShot();
        } else {
            screenShot();
        }
    }

    public static void DowaitandDismisalertwhen(int timeOut) throws InterruptedException {


    }

    //**********************Send Keys ********************
    public static void DosendKeyRRWhenReady(String locator, String actualText, String ObjectName, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement mcj = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        isElementDisplayedandEnabled(locator, ObjectName, timeOut);
        mcj.clear();
        mcj.sendKeys(actualText);
    }

    public static void DoSendKeysWhenReady(String locator, String ObjectName, String actualText, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        isElementDisplayedandEnabled(locator, ObjectName, timeOut);
        element.clear();
        element.sendKeys(Utility.fetchLocator(actualText));
    }

    public static void DosendKeysRandomEmailsWhenReady(String locator, String ObjectName, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        Randomstuff randomstuff = new Randomstuff();
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        isElementDisplayedandEnabled(locator, ObjectName, timeOut);
        element.clear();
        String a = "@gmail.com";
        element.sendKeys(ListRandom() + a);
    }

    public static void DosendKeysRandomNumberWhenReady(String locator, int span, int timeOut) throws IOException, InterruptedException {
        SecureRandom rn = new SecureRandom();
        int resourcetype = rn.nextInt(span) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        element.clear();
        String a = "";
        element.sendKeys(a + resourcetype);
    }

    public static void DosendKeysRandomListwordsWhenReady(String locator, String ObjectName, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1100);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        isElementDisplayedandEnabled(locator, ObjectName, timeOut);
        element.clear();
        String a = "";
        element.sendKeys(a + ListRandom());
    }

    public static void DoSendKeysRobotClassFluentWait(String locator, String actualText, int timeOut) throws IOException, InterruptedException, AWTException {
        Robot r = new Robot();
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        element.click();
        Actions actionp = new Actions(getDriver());
        actionp.moveToElement(element).doubleClick().perform();

        StringSelection stringSelection = new StringSelection(Utility.fetchLocator(actualText));
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, stringSelection);

        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_V);
        r.keyRelease(KeyEvent.VK_V);
        r.keyRelease(KeyEvent.VK_CONTROL);

        Thread.sleep(2000);
        r.keyPress(KeyEvent.VK_ENTER);
    }

    //**********************Clicks ********************
    public static void DoclickFluentWait(String locator, String ObjectName, int timeOut) throws IOException, InterruptedException {
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        element.click();
        System.out.println("waited for " + Utility.fetchLocator(ObjectName) + " to be present on the page -->" + timeOut + " milliseconds");
    }

    public static void DoclickWhenReady(String locator, String ObjectName, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        element.click();
    }


    //********************drop down utils **************************
    public static void doClickFromList(WebDriver driver, String locator, String linkText) throws IOException {
        java.util.List<WebElement> footerList = driver.findElements(By.xpath(Utility.fetchLocator(locator)));
        for (int i = 0; i < footerList.size(); i++) {
            String text = footerList.get(i).getText();
            if (text.equals(linkText)) {
                footerList.get(i).click();
                break;
            }
        }
    }

    public static void DoSelectValuesByVisibleText(String locator, String value, String ObjectName, int timeOut) throws IOException, InterruptedException {
        WebElement locat = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        isElementDisplayedandEnabled(locator, ObjectName, timeOut);
        Select select = new Select(locat);
        select.selectByVisibleText(Utility.fetchLocator(value));
    }

    public static void DoSelectValuesByIndex(String locator, String ObjectName, int index, int timeOut) throws IOException, InterruptedException {
        WebElement locat = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        isElementDisplayedandEnabled(locator, ObjectName, timeOut);
        Select select = new Select(locat);
        select.selectByIndex(index);
    }

    public static void DoSelectValuesByValue(String locator, String ObjectName, String value, int timeOut) throws IOException, InterruptedException {
        WebElement locat = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        isElementDisplayedandEnabled(locator, ObjectName, timeOut);
        Select select = new Select(locat);
        select.selectByValue(Utility.fetchLocator(value));
    }

    public static String DoGetPageCurrentUrl(int timeOut, String urlValue) throws IOException, InterruptedException {
        Thread.sleep(1100);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        wait.until(ExpectedConditions.urlContains(Utility.fetchLocator(urlValue)));
        return getDriver().getCurrentUrl();
    }


    //***************************wait utils ******************************
    public static WebElement DowaitForElementPresent(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1100);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        return wait.until(ExpectedConditions.presenceOfElementLocated((By) getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
    }

    public static WebElement DowaitForElementToBeVisible(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1100);
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static Alert DowaitForAlertPresent(int timeOut) throws InterruptedException {
        Thread.sleep(1100);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        return wait.until(ExpectedConditions.alertIsPresent());
    }

    //**********************Displayed and Enabled ********************
    public static boolean isElementDisplayed(String locator, String ObjectName, int timeout) throws InterruptedException, IOException {
        Thread.sleep(500);
        WebElement element = null;
        boolean flag = false;
        for (int i = 0; i < timeout; i++) {

            try {
                element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
                flag = element.isDisplayed();
                break;
            } catch (Exception e) {
                System.out.println("waiting for element to be present on the page -->" + i + "secs");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                }
            }

        }
        return flag;

    }

    public static boolean isElementDisplayedandEnabled(String locator, String ObjectName, int timeout) throws InterruptedException, IOException {
        Thread.sleep(500);
        WebElement element = null;
        boolean flag = false;
        for (int i = 0; i < timeout; i++) {

            try {
                element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
                flag = element.isDisplayed() && element.isEnabled();
                break;
            } catch (Exception e) {
                System.out.println("waiting for element to be present on the page --> " + i + " secs");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                }
            }

        }
        return flag;
    }


    public boolean isElementEnabled(String locator, String ObjectName, int timeout) throws InterruptedException, IOException {
        Thread.sleep(500);
        WebElement element = null;
        boolean flag = false;
        for (int i = 0; i < timeout; i++) {

            try {
                element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
                flag = element.isDisplayed() && element.isEnabled();
                break;
            } catch (Exception e) {
                System.out.println("waiting for element to be present on the page -->" + i + "secs");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                }
            }
        }
        return flag;
    }

    public static int getResponseCode( String locator) throws IOException {
        return Integer.parseInt(getDriver().findElement(By.xpath(Utility.fetchLocator(locator))).getAttribute("content"));
    }

    //******************* FluentWait Utils ***********************
    public WebElement DowaitForElementWithFluentWaitConcept(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(500);
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class);

        return wait.until(ExpectedConditions.presenceOfElementLocated((By) getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
    }

    public static WebElement DoFluentWait(final String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(500);
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                try {
                    return getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        return element;
    }

    public static String Password(int minimumLength, int maximumLength, boolean includeUppercase, boolean includeSpecial) {
        Faker faker = new Faker();
        if (includeSpecial) {
            char[] password = faker.lorem().characters(minimumLength, maximumLength, includeUppercase).toCharArray();
            char[] special = new char[]{'!', '@', '#', '$', '%', '^', '&', '*'};
            for (int i = 0; i < faker.random().nextInt(minimumLength); i++) {
                password[faker.random().nextInt(password.length)] = special[faker.random().nextInt(special.length)];
            }
            return new String(password);
        } else {
            return faker.lorem().characters(minimumLength, maximumLength, includeUppercase);
        }
    }


    public Sheet getSpreadSheet() {
        File file = new File("//Path//To//Test.xlsx");

        FileInputStream inputStream = null;
        Workbook wb = null;
        try {
            inputStream = new FileInputStream(file);
            wb = WorkbookFactory.create(inputStream);
            System.out.println(wb.toString());
        } catch (IOException ex) {
            System.out.println("Error Message " + ex.getMessage());
        } catch (org.apache.poi.openxml4j.exceptions.InvalidFormatException e) {
            e.printStackTrace();
        }

        Sheet mySheet = wb.getSheet("MySheet");

        return mySheet;
    }
    public void loginPage() {
        Sheet mySheet = getSpreadSheet();
        String user = mySheet.getRow(0).getCell(0).toString();
        String pass = mySheet.getRow(0).getCell(1).toString();

        user = mySheet.getRow(1).getCell(0).toString();
        pass = mySheet.getRow(1).getCell(1).toString();
    }

}
