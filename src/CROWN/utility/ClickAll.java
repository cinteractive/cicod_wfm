package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.awt.*;
import java.io.IOException;
import java.util.List;

public class ClickAll extends TestBase {

    public static void clickall(String locator) throws IOException, AWTException, InterruptedException {
        List<WebElement> checkBoxList = getDriver().findElements(By.className(Utility.fetchLocator(locator)));
        checkBoxList.forEach(ele -> ele.click());
    }

    public static void selectAllCheckboxes(String locator) throws IOException {

        List<WebElement> AllCheckboxes = getDriver().findElements(By.className(locator));

        int size = AllCheckboxes.size();
        System.out.println(size);

        for (int i = 0; i < size; i++) {

            AllCheckboxes.get(i).click();

        }
    }
}