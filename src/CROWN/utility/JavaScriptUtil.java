package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class JavaScriptUtil extends TestBase {

    public static boolean isElementPresentCheckUsingJavaScriptExecutor(WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        try {
            Object obj = jse.executeScript("return typeof(arguments[0]) != 'undefined' && arguments[0] != null;",
                    element);
            if (obj.toString().contains("true")) {
                System.out.println("isElementPresentCheckUsingJavaScriptExecutor: SUCCESS");
                return true;
            } else {
                System.out.println("isElementPresentCheckUsingJavaScriptExecutor: FAIL");
            }

        } catch (NoSuchElementException e) {
            System.out.println("isElementPresentCheckUsingJavaScriptExecutor: FAIL");
        }
        return false;
    }


    public static Object getPageLoadTime2() {
        //Creating the JavascriptExecutor interface object by Type casting
        JavascriptExecutor js = (JavascriptExecutor) getDriver();

        //This will get you the time passed since you page navigation started
        return ((JavascriptExecutor) getDriver()).executeScript("return Date.now() - performance.timeOrigin;");
    }

    public static void DoscrolltoViewClickFluentWait(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement ti11 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].scrollIntoView();", ti11);
        ti11.click();
    }

    public static void DoscrolltoView(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement ti11 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].scrollIntoView();", ti11);
        ti11.click();
    }

    public static void DoClickWhenReadyJS(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement ele = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", ele);
    }

    public static void DoZoomPercentage(int Percentage) throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("document.body.style.zoom = '" + Percentage + "%" + "';");
    }

    public static void DoscrollIntoView(String locator) throws InterruptedException, IOException {
        Thread.sleep(800);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView(true);", Utility.fetchLocator(locator));
        Thread.sleep(600);
    }
}
