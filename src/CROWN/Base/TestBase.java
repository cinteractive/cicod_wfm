package CROWN.Base;

import CROWN.Listeners.ExtentReport;
import CROWN.Listeners.WebEventListener;
import CROWN.utility.Utility;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TestBase extends ExtentReport {

    public static ThreadLocal<WebDriver> getdriver = new ThreadLocal<>();

    public static synchronized WebDriver getDriver() {
        return getdriver.get();
    }

    EventFiringWebDriver eDriver;
    WebEventListener eventListener;

    @BeforeTest(alwaysRun = true)
    public void SetUp() throws IOException {
        switch (Utility.fetchProperty("BrowserName").toString()) {
            case "Chrome":
                WebDriverManager.chromedriver().setup();
                getdriver.set(new ChromeDriver());
                break;

            case "Firefox":
                WebDriverManager.firefoxdriver().setup();
                getdriver.set(new FirefoxDriver());
                break;

            case "ChromeHeadless":
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--no-sandbox");
                chromeOptions.addArguments("--headless");
                getdriver.set(new ChromeDriver(chromeOptions));
                break;

            case "FirefoxHeadless":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions option = new FirefoxOptions();
                option.addArguments("--headless");
                option.addArguments("--incognito");
                getdriver.set(new FirefoxDriver(option));
                break;

            case "RemoteFirefox":
                FirefoxOptions caps = new FirefoxOptions();
                caps.setCapability("browserName", "firefox");
                caps.setCapability("enableVNC", true);
                caps.setCapability("console", true);
                caps.setCapability("tunnel", true);

                try {
                    getdriver.set(new RemoteWebDriver(new URL(Utility.fetchProperty("RemoteURL").toString()), caps));

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                break;

            case "RemoteChrome":
                ChromeOptions cap = new ChromeOptions();
                cap.setCapability("browserName", "chrome");
                cap.setCapability("enableVNC", true);
                cap.setCapability("console", true);
                cap.setCapability("tunnel", true);

                try {
                    getdriver.set(new RemoteWebDriver(new URL(Utility.fetchProperty("RemoteURL").toString()), cap));

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }


                break;
        }

        eDriver = new EventFiringWebDriver(getdriver.get());
        eventListener = new WebEventListener();
        eDriver.register(eventListener);
        getdriver.set(eDriver);

        getdriver.get().manage().window().maximize();

        try {
            getdriver.get().get(Utility.fetchProperty("UrlPROD").toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        getdriver.get().manage().timeouts().pageLoadTimeout(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        getdriver.get().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);

    }


    public static Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }

    @AfterTest(alwaysRun = true)
    public void Quit() {

        try {
            if (getdriver.get() != null)
                getdriver.get().quit();
        } catch (Exception ignored) {
        }
    }
}