package CROWN.CICOD.WFM.QueueManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.isAlertPresent;

public class UpdateQUEUE extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue Type")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ManageQueueType() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageQueueType_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("ActionQUEBTN_XPATH", 60);
    }

    @Description("Update Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        doclickWhenReady("queueUpdateBTN_XPATH", 20);
    }

    @Description("Queue Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void QueueDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("QueueDescription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Save Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void SaveQueue() throws IOException, InterruptedException {
        doclickWhenReady("QueueSaveBTN_XPATH", 20);
        isAlertPresent();
    }

    @Description("Assert Save Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertSaveQueue() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdateQueue_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OK_XPATH", 20);
    }
}
