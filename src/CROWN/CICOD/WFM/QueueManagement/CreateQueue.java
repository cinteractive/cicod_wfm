package CROWN.CICOD.WFM.QueueManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateQueue extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ManageQueueType() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageQueueType_XPATH", 60);
    }

    @Description("New Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewQueue() throws IOException, InterruptedException {
        Thread.sleep(3000);
        DoClickWhenReadyJS("NewQQQ_XPATH", 60);
    }

    @Description("Queue Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void QueueName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("QueueName_XPATH", 20);
    }

    @Description("Queue Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void QueueDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("QueueDescription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Create Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Createqueue() throws IOException, InterruptedException {
        doclickWhenReady("QueueCreateBTN_XPATH", 20);
    }

    @Description("Assert Create Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreateQueue() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AsertCreateQUE_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTNZ_XPATH", 20);
    }
}
