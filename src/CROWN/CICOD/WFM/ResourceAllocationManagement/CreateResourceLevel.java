package CROWN.CICOD.WFM.ResourceAllocationManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateResourceLevel extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Resource Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void ResourceManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ResourceManagement_XPATH", 60);
    }

    @Description("Manage Resource Level")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ManageResourceLevel() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageResouceLevel_XPATH", 60);
    }

    @Description("New Resource Level")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewResourceLevel() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("NewResouceLevel_XPATH", 60);
    }

    @Description("Resource Level Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void ResourceLevelName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("ResourceLevelName_XPATH", 20);
    }

    @Description("Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void DepartmentDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("ResourceLevelDiscription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Create Resource Level")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void CreateResourcelevel() throws IOException, InterruptedException {
        doclickWhenReady("CreateResourceLevelBTN_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("ResourceLevelOKBTN_XPATH", 20);
    }
}
