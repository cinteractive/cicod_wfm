package CROWN.CICOD.WFM.ResourceAllocationManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;
import static CROWN.utility.Utility.DowaitandAcceptAlerwhenReady;

public class Suspend_UnsuspendResourceLevel extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Resource Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void ResourceManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ResourceManagement_XPATH", 60);
    }

    @Description("Manage Resource Level")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ManageResourceLevel() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageResouceLevel_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("ActionUpdateResourLevel_XPATH", 60);
    }

    @Description("Suspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Suspend() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SuspendResourceLevel_XPATH", 60);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("ResoOKBTN_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Action2() throws IOException, InterruptedException {
        this.Action();
    }

    @Description("Unsuspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void Unsuspend() throws IOException, InterruptedException {
        doclickWhenReady("UnsuspendResourceLevel_XPATH", 20);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void OK2() throws IOException, InterruptedException {
        this.OK();
    }
}
