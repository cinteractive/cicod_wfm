package CROWN.CICOD.WFM.ResourceAllocationManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateNewResource extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        doclickWhenReady("Wfm_XPATH", 20);
    }

    @Description("Resource Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void ResourceManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ResourceManagement_XPATH", 21);
    }

    @Description("Manage Resource")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Users() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageResourceBTN_XPATH", 20);
    }

    @Description("New Resources")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewResources() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("NewREsourcs_XPATH", 20);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Name() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DosendKeysRandomListwordsWhenReady("ResourceFirstName_XPATH", 20);
        DosendKeysRandomListwordsWhenReady("ResourceLastName_XPATH", 20);
    }

    @Description("Email")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Email() throws IOException, InterruptedException {
        DosendKeysRandomEmailsWhenReady("ResourceEmail_XPATH", 20);
    }

    @Description("PhoneNumber")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void PhoneNumber() throws IOException, InterruptedException {
        DoSendKeysWhenReady("ResourcePhoneNumber_XPATH", "ContactPhoneNumber_TEXT", 20);
    }

    @Description("Staff ID")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void StaffID() throws IOException, InterruptedException, AWTException {
        DosendKeysRandomNumberWhenReady("ResourceStaffID_XPATH", 500000, 60);
    }

    @Description("Job Tittle")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void JobTittle() throws IOException, InterruptedException, AWTException {
        DosendKeysRandomListwordsWhenReady("JobTitle_XPATH", 50);
    }

    @Description("Next Of Kin Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void NextOfKinName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("NextOfKin_XPATH", 20);
    }

    @Description("Next of Kin Phone Number")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void NextOfKinPhoneNumber() throws IOException, InterruptedException {
        DosendKeyRRWhenReady("NextOfKinPhoneNumber_XPATH", "ContactPhoneNumber_TEXT", 20);
    }

    @Description("Address")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 14)
    public void Address() throws IOException, InterruptedException {
        getDriver().findElement(By.xpath(Utility.fetchLocator("ResourceAddressLine1_XPATH"))).sendKeys(Utility.fetchLocator("AddressLine1_TEXT"));
        Thread.sleep(1200);
        getDriver().findElement(By.xpath(Utility.fetchLocator("ResourceAddressLine2_XPATH"))).sendKeys(Utility.fetchLocator("AddressLine1_TEXT"));
    }

    @Description("City")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 15)
    public void City() throws IOException, InterruptedException {
        DosendKeyRRWhenReady("City_XPATH", "City_TEXT", 20);
    }

    @Description("State")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 16)
    public void State() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("State_XPATH", 20, 20);
    }

    @Description("Resource Type")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 17)
    public void ResourceType() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("ResourceType_XPATH", 5, 20);
    }

    @Description("Resource Level")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 18)
    public void ResourceLevel() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("ResourceLevel_XPATH", 5, 20);
    }

    @Description("Resource Schedule")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 19)
    public void ResourceSchedule() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("ResourceSdedule_XPATH", 5, 20);
    }

    @Description("Join Date")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 20)
    public void JoinDate() throws IOException, InterruptedException {
        getDriver().findElement(By.id("Engineer_date_joined")).click();
        getDriver().findElement(By.xpath("//tr[4]/td[4]")).click();
    }

    @Description("Resource Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 21)
    public void ResourceDepartment() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("ResourceDepartment_XPATH", 50);
        doclickWhenReady("BillingDepartment_XPATH", 50);
        doclickWhenReady("EngineeringDepartment_XPATH", 50);
    }

    @Description("Create New Resource")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 22)
    public void CreateUser() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("CreateResourceBTN_XPATH", 50);
    }

    @Description("Assert Create New Resource")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 23)
    public void AssertCreateUser() throws IOException, InterruptedException, AWTException {
        DoAssertContainsWhenReady("AssertResourcesCreation_XPATH", "CNR_TEXT", 30);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 24)
    public void OK() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("CreateOKBTN_XPATH", 20);
    }
}
