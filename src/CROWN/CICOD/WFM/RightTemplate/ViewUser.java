package CROWN.CICOD.WFM.RightTemplate;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class ViewUser extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        doclickWhenReady("Wfm_XPATH", 20);
    }

    @Description("User Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void UserManagement() throws IOException, InterruptedException {
        doclickWhenReady("UserManagement_XPATH", 21);
    }

    @Description("Users")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Users() throws IOException, InterruptedException {
        doclickWhenReady("Usr_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("UserActionBTNN_XPATH", 60);
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void ViewUser() throws IOException, InterruptedException {
        doclickWhenReady("viewUser_XPATH", 20);
    }

    @Description("Assert View User")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void AssertViewUser() throws IOException, InterruptedException {
        Thread.sleep(7000);
        CheckElementPresent("pi_XPATH",20);
    }
}
