package CROWN.CICOD.WFM.RightTemplate;

import CROWN.Base.TestBase;
import CROWN.utility.ExcelUtil;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.FileUpload.DoFileUpWhenReady;
import static CROWN.utility.FileUpload.UploadFileImage3MB;

public class CreateUsers extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        doclickWhenReady("Wfm_XPATH", 50);
    }

    @Description("User Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void UserManagement() throws IOException, InterruptedException {
        doclickWhenReady("UserManagement_XPATH", 21);
    }

    @Description("Users")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Users() throws IOException, InterruptedException {
        doclickWhenReady("Usr_XPATH", 20);
    }

    @Description("Users Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void CreateUsers() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("ui_XPATH", 20);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Name() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("NewUserFname_XPATH", 20);
        DosendKeysRandomListwordsWhenReady("NewUserLastName_XPATH", 20);
    }

    @Description("Email")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Email() throws IOException, InterruptedException {
        DosendKeysRandomEmailsWhenReady("NewUserEmail_XPATH", 20);
    }

    @Description("PhoneNumber")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void PhoneNumber() throws IOException, InterruptedException {
        ExcelUtil.DoSendKeysWhenReadyPhoneNumber("NewUserPhone_XPATH", 20);
    }

    @Description("Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void RightTemplate() throws IOException, InterruptedException {
        DoSelectValuesByIndex("NewUserRightTemplate_XPATH", 3, 20);
    }

    @Description("Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Department() throws IOException, InterruptedException {
        doclickWhenReady("NewUserDepartment_XPATH", 20);
        doclickWhenReady("NewUserA1_XPATH", 20);
        doclickWhenReady("NewUserA2_XPATH", 20);
        doclickWhenReady("Cll_XPATH", 20);
    }

    @Description("Staff ID")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void StaffID() throws IOException, InterruptedException, AWTException {
        DosendKeysRandomNumberWhenReady("NewUserStaffID_XPATH", 500000, 60);
    }

    @Description("Job Tittle")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void JobTittle() throws IOException, InterruptedException, AWTException {
        DosendKeysRandomListwordsWhenReady("NewserJobTittle_XPATH", 50);
        doclickWhenReady("Cll_XPATH", 20);
    }

    @Description("Region")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 14)
    public void Region() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("NewUserRegion_XPATH", 50);
        doclickWhenReady("Newq11_XPATH", 50);
        doclickWhenReady("Cll_XPATH", 60);
    }

    @Description("Create User")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 15)
    public void CreateUser() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("CreateNewUser_XPATH", 50);
    }

    @Description("Assert Create User")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 16)
    public void AssertCreateUser() throws IOException, InterruptedException, AWTException {
        DoAssertContainsWhenReady("AssertNewUserCreation_XPATH", "Su_TEXT", 30);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 17)
    public void OK() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("NewUserOKBTN_XPATH", 20);
    }
}
