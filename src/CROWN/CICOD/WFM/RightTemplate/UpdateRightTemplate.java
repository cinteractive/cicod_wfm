package CROWN.CICOD.WFM.RightTemplate;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class UpdateRightTemplate extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        doclickWhenReady("Wfm_XPATH", 20);
    }

    @Description("User Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void UserManagement() throws IOException, InterruptedException {
        doclickWhenReady("UserManagement_XPATH", 21);
    }

    @Description("Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RightTemplate() throws IOException, InterruptedException {
        doclickWhenReady("RightTemplateBTN_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);

        DoClickWhenReadyJS("RightTemplateActionBTN_XPATH", 60);
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        doclickWhenReady("UpdateRightTemplate_XPATH", 20);
    }

    @Description("Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Description() throws IOException, InterruptedException {
        DoSendKeysWhenReady("RightTemplateDiscription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Save")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Save() throws IOException, InterruptedException {
        doclickWhenReady("CreateRightTemplateBTN_XPATH", 20);
    }

    @Description("Assert Save")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertSave() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdateRightTemplateBTN_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("UpdateRightTemplateOKBTN_XPATH", 20);
    }
}
