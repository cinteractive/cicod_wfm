package CROWN.CICOD.WFM.RightTemplate;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateRightTemplate extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("User Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void UserManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UserManagement_XPATH", 60);
    }

    @Description("Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RightTemplate() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RightTemplateBTN_XPATH", 60);
    }

    @Description("New Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewRightTemplate() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("NewRightTemplateBTN_XPATH", 60);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Name() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("RightTemplateName_XPATH", 20);
    }

    @Description("Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Description() throws IOException, InterruptedException {
        DoSendKeysWhenReady("RightTemplateDiscription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Create Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void CreateRightTemplate() throws IOException, InterruptedException {
        doclickWhenReady("CreateRightTemplateBTN_XPATH", 20);
    }

    @Description("Assert Create")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreate() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertRightTemolateCreation_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("RightTemplatOKBTN_XPATH", 20);
    }

}
