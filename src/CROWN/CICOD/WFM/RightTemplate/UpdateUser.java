package CROWN.CICOD.WFM.RightTemplate;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class UpdateUser extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        doclickWhenReady("Wfm_XPATH", 20);
    }

    @Description("User Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void UserManagement() throws IOException, InterruptedException {
        doclickWhenReady("UserManagement_XPATH", 21);
    }

    @Description("Users")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Users() throws IOException, InterruptedException {
        doclickWhenReady("Usr_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("UserActionBTNN_XPATH", 60);
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        doclickWhenReady("UpdateBtton_XPATH", 20);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Name() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DosendKeysRandomListwordsWhenReady("NewUserFname_XPATH", 20);
        DosendKeysRandomListwordsWhenReady("NewUserLastName_XPATH", 20);
    }

    @Description("Right Template")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void RightTemplate() throws IOException, InterruptedException {
        DoSelectValuesByIndex("NewUserRightTemplate_XPATH", 3, 20);
    }

    @Description("Region")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void Region() throws IOException, InterruptedException, AWTException {
        doclickWhenReady("Ri_XPATH", 50);
        doclickWhenReady("Rii_XPATH", 50);
        doclickWhenReady("Cll_XPATH", 60);
    }

    @Description("Update User")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void UpdateUser() throws IOException, InterruptedException {
        doclickWhenReady("UserU_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("NewUserOKBTN_XPATH", 20);
    }
}
