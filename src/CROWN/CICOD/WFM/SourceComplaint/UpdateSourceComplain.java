package CROWN.CICOD.WFM.SourceComplaint;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;
import static CROWN.utility.Utility.DowaitandAcceptAlerwhenReady;

public class UpdateSourceComplain extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Source Complain")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void SourceComplain() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SourceComplainBTN_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        getDriver().findElement(By.name("yt1")).click();
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        doclickWhenReady("UpdateSourceComplainBTN_XPATH", 20);
    }

    @Description("Source Complaint Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void SourceComplaintDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("SourceComplaintDescription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Update Source Complain")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void UpdateSourceComplain() throws IOException, InterruptedException {
        doclickWhenReady("CreateQUEUEbtn_XPATH", 20);
        java.lang.Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN4_XPATH", 20);
    }
}
