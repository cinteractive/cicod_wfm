package CROWN.CICOD.WFM.SourceComplaint;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.isAlertPresent;

public class CreateSourceComplaint extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Source Complain")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void SourceComplain() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SourceComplainBTN_XPATH", 60);
    }

    @Description("Add Source Complain Source")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void AddSourceComplainSource() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AddSourceComplainSource_XPATH", 60);
    }

    @Description("Source Complaint Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void SourceComplaintName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("SourceComplaintName_XPATH", 20);
    }

    @Description("Source Complaint Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void SourceComplaintDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("SourceComplaintDescription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Create Source Complain")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void CreateSourceComplain() throws IOException, InterruptedException {
        doclickWhenReady("CreateQUEUEbtn_XPATH", 20);
        isAlertPresent();
    }

    @Description("Assert Create Source Complain")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreateSourceComplain() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertSourceComplaintCreation_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN4_XPATH", 20);
    }
}
