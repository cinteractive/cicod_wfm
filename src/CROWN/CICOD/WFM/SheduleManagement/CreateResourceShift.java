package CROWN.CICOD.WFM.SheduleManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import com.google.api.services.gmail.model.Thread;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DosendKeysRandomListwordsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateResourceShift extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Schedule Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void ScheduleManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ScheduelManagment_XPATH", 60);
    }

    @Description("Resource Shift")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ResourceShift() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ResourcShiftBTN_XPATH", 60);
    }

    @Description("New Resource Shift")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewResourceShift() throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        DoClickWhenReadyJS("NewRsourceShiftBTN_XPATH", 60);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Name() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("ShiftName_XPATH", 20);
    }

    @Description("Colour")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Colour() throws IOException, InterruptedException {
        doclickWhenReady("ShiftColor_XPATH", 20);
        TimeUnit.SECONDS.sleep(5);
        doclickWhenReady("ColorClicker_XPATH", 20);
        TimeUnit.SECONDS.sleep(5);
        doclickWhenReady("aahl_XPATH", 20);
    }

    @Description("Create")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Create() throws IOException, InterruptedException {
        doclickWhenReady("dummy123_XPATH",10);
        doclickWhenReady("ShiftCreateBTN_XPATH", 20);
    }

    @Description("Assert Create")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreate() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertResourceShift_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN1_XPATH", 20);
    }
}
