package CROWN.CICOD.WFM.SheduleManagement;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DosendKeysRandomListwordsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.Login;

public class UpdateResourceShift extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Schedule Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void ScheduleManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ScheduelManagment_XPATH", 60);
    }

    @Description("Resource Shift")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ResourceShift() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ResourcShiftBTN_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("RsourceShiftActionBTN_XPATH", 60);
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        doclickWhenReady("UpdateShiftBTN_XPATH", 20);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Name() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("ShiftName_XPATH", 20);
    }

    @Description("Save")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Save() throws IOException, InterruptedException {
        doclickWhenReady("ShiftCreateBTN_XPATH", 20);
    }

    @Description("Assert Save")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertSave() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdateResourceShift_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN1_XPATH", 20);
    }
}