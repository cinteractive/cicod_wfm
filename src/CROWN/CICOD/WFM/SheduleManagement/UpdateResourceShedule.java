package CROWN.CICOD.WFM.SheduleManagement;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.Login;

public class UpdateResourceShedule extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Schedule Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void ScheduleManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ScheduelManagment_XPATH", 60);
    }

    @Description("Schedule Record")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ScheduleRecord() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SheduleRecordBTN_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("ResourceSheduleActionBTN_XPATH", 60);
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        doclickWhenReady("UpdateResourceSheduleTimer_XPATH", 20);
    }

    @Description("Save Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void SaveUpdate() throws IOException, InterruptedException {
        doclickWhenReady("ShiftCreateBtn", 20);
    }

    @Description("Assert Save")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertSave() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdateResourceShift_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN3_XPATH", 20);
    }
}
