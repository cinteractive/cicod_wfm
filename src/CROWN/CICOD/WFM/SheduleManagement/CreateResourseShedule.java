package CROWN.CICOD.WFM.SheduleManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateResourseShedule extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Schedule Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void ScheduleManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ScheduelManagment_XPATH", 60);
    }

    @Description("Schedule Record")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ScheduleRecord() throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        DoClickWhenReadyJS("SheduleRecordBTN_XPATH", 60);
    }

    @Description("New Schedule Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewScheduleManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("NewSheduleManagement_XPATH", 60);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Name() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("SheduleName_XPATH", 20);
    }

    @Description("Monday")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Monday() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Monday_XPATH", 3, 20);
    }

    @Description("Tuesday")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Tuesday() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Tuesday_XPATH", 3, 20);
    }

    @Description("Wednesday")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Wednesday() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Wednesday_XPATH", 3, 20);
    }

    @Description("Thursday")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void Thursday() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Thursday_XPATH", 3, 20);
    }

    @Description("Friday")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Friday() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Friday_XPATH", 3, 20);
    }

    @Description("Saturday")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void Saturday() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Saturday_XPATH", 3, 20);
    }

    @Description("Create")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void Create() throws IOException, InterruptedException {
        doclickWhenReady("ShiftCreateBtn", 20);
    }

    @Description("Assert Create")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void AssertCreate() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertResourceCreation_XPATH", "Successful_TEXT",  20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 14)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN3_XPATH", 20);
    }
}
