package CROWN.CICOD.WFM.ManageWorkOrderStatus;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class UpdateStatus extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Status() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageWorkOrderStatus_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("StatusActionBTN_XPATH", 60);
    }

    @Description("Update Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Update() throws IOException, InterruptedException {
        doclickWhenReady("UpdateStatus_XPATH", 20);
    }

    @Description("Department Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void StatusDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("Rdiscription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Update Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void UpdateStatus() throws IOException, InterruptedException {
        doclickWhenReady("FormCreateBTN_XPATH", 20);
    }

    @Description("Assert Update Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertUpdateStatus() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdateStatusa_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKWK_XPATH", 20);
    }
}
