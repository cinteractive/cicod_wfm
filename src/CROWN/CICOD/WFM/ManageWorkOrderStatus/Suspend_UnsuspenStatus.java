package CROWN.CICOD.WFM.ManageWorkOrderStatus;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;
import static CROWN.utility.Utility.DowaitandAcceptAlerwhenReady;

public class Suspend_UnsuspenStatus extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Status() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageWorkOrderStatus_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("StatusActionBTN_XPATH", 60);
    }

    @Description("Suspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Suspend() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Suspend_XPATH", 60);
        Thread.sleep(3000);
        AcceptAlert();    }

    @Description("Assert Suspend Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void AssertSuspendDepartment() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("SuspendStatus_XPATH", "Suspended_TEXT",20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("OKsus_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Action2() throws IOException, InterruptedException {
        this.Action();
    }

    @Description("UnsuspendEscalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void UnsuspendDepartment() throws IOException, InterruptedException {
        doclickWhenReady("unsuspend_XPATH", 20);
        Thread.sleep(3000);
        AcceptAlert();    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void OK2() throws IOException, InterruptedException {
        this.OK();
    }
}
