package CROWN.CICOD.WFM.ManageWorkOrderStatus;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateStatus extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage WorkOrder Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void ManageWorkOrderStatus() throws IOException, InterruptedException {
        Thread.sleep(3000);
        DoClickWhenReadyJS("ManageWorkOrderStatus_XPATH", 60);
    }

    @Description("New Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewStatus() throws IOException, InterruptedException {
        DoClickWhenReadyJS("NewRecordBTN_XPATH", 60);
    }

    @Description("Status Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void StatusName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("Form_XPATH", 20);
    }

    @Description("Status Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void StatusDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("Rdiscription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Create Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void CreateStatus() throws IOException, InterruptedException {
        doclickWhenReady("FormCreateBTN_XPATH", 20);
    }

    @Description("Assert Create Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreateDepartment() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertStatusCreation_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKCreat_XPATH", 20);
    }

}
