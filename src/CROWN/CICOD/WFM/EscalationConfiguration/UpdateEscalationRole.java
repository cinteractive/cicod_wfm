package CROWN.CICOD.WFM.EscalationConfiguration;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class UpdateEscalationRole extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Escalation Main Menu")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void EscalationMainMenu() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationMainMenue_XPATH", 60);
    }

    @Description("Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void EscalationToRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationTorole_XPATH", 60);
    }

    @Description("Action Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void ActionEscalationToRole() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("ActionBTNEscalationToRole_XPATH", 60);
    }

    @Description("Update Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void UpdateEscalationToRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UpdateEscalationRoleBTN_XPATH", 60);
    }

    @Description("Escalation Role Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void EscalationRoleDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("EscalationRoleDiscription_XPATH", "Decrib_TEXT",60);
    }

    @Description("Update Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void UpdateEscalation() throws IOException, InterruptedException {
        doclickWhenReady("CreateEscalationBTN_XPATH",60);
    }

    @Description("Assert Update Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertUpdateEscalation() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdateEscal_XPATH","Successful_TEXT",20);
    }
}
