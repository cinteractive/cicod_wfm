package CROWN.CICOD.WFM.EscalationConfiguration;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateNewRegionToEscalation extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Escalation Main Menu")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void EscalationMainMenu() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationMainMenue_XPATH", 60);
    }

    @Description("Region To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RegionToRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RegionToRoleBTN_XPATH", 20);
    }

    @Description("New Region To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewRegionToRole() throws IOException, InterruptedException {
        Thread.sleep(4000);
        DoClickWhenReadyJS("NewRegionToRoleBTN_XPATH", 60);
    }

    @Description("Regions")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Regions() throws IOException, InterruptedException {
        doclickWhenReady("RegionsBTN_XPATH", 60);
    }

    @Description("IbejuRegion")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void IbejuRegion() throws IOException, InterruptedException {
        doclickWhenReady("IbejuRegion_XPATH", 21);
        doclickWhenReady("Clicker_XPATH", 21);
    }

    @Description("Escalation Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void EscalationRole() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("escalRole_XPATH", 5, 20);
    }

    @Description("Escalation Email")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void EscalationEmail() throws IOException, InterruptedException {
        DosendKeysRandomEmailsWhenReady("RegionEmail_XPATH", 20);
    }

    @Description("Create Region To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void CreateRegionToRole() throws IOException, InterruptedException {
        doclickWhenReady("CreateRegionToRole_XPATH", 20);
    }

    @Description("Assert Create Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void AssertCreateEscalation() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssrtNewRegion_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN5_XPATH", 20);
    }
}
