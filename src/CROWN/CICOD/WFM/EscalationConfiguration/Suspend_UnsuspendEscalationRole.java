package CROWN.CICOD.WFM.EscalationConfiguration;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;
import static CROWN.utility.Utility.isAlertPresent;

public class Suspend_UnsuspendEscalationRole extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Escalation Main Menu")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void EscalationMainMenu() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationMainMenue_XPATH", 60);
    }

    @Description("Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void EscalationToRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationTorole_XPATH", 60);
    }

    @Description("Action Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void ActionEscalationToRole() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("ActionBTNEscalationToRole_XPATH", 60);
    }

    @Description("Suspend Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void SuspendEscalation() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SuspendEscalation_XPATH", 60);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void OK() throws IOException, InterruptedException {
        java.lang.Thread.sleep(3000);
        AcceptAlert();
        java.lang.Thread.sleep(3000);
        doclickWhenReady("OKESc_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Action2() throws IOException, InterruptedException {
        this.ActionEscalationToRole();
    }

    @Description("UnsuspendEscalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void UnsuspendEscalation() throws IOException, InterruptedException {
        doclickWhenReady("UnsuspendEscalation_XPATH", 20);
    }

    @Description("EscalationAlert")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void EscalationAlert() throws IOException, InterruptedException {
        java.lang.Thread.sleep(3000);
        AcceptAlert();
        java.lang.Thread.sleep(3000);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void OK2() throws IOException, InterruptedException {
        doclickWhenReady("OKESc_XPATH", 20);
    }
}
