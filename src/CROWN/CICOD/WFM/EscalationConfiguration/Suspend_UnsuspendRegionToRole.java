package CROWN.CICOD.WFM.EscalationConfiguration;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;
import static CROWN.utility.Utility.DowaitandAcceptAlerwhenReady;

public class Suspend_UnsuspendRegionToRole extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Escalation Main Menu")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void EscalationMainMenu() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationMainMenue_XPATH", 60);
    }

    @Description("Region To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RegionToRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RegionToRoleBTN_XPATH", 20);
    }

    @Description("Action Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("RegiontoRoleActionBTN_XPATH", 60);
    }

    @Description("Suspend Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void SuspendEscalation() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SuspendEscalationRole_XPATH", 60);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("Assert Suspend Region Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void AssertSuspendRegionRole() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertRoleSuspension_XPATH", "Suspended_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN6_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void Action2() throws IOException, InterruptedException {
        this.Action();
    }

    @Description("UnsuspendEscalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void UnsuspendEscalation() throws IOException, InterruptedException {
        doclickWhenReady("UnsuspendEscalationRole_XPATH", 20);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 15)
    public void OK2() throws IOException, InterruptedException {
        this.OK();
    }
}
