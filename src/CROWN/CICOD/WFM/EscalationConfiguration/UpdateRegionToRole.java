package CROWN.CICOD.WFM.EscalationConfiguration;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.DosendKeysRandomEmailsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class UpdateRegionToRole extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Escalation Main Menu")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void EscalationMainMenu() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationMainMenue_XPATH", 60);
    }

    @Description("Region To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void RegionToRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("RegionToRoleBTN_XPATH", 20);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        DoClickWhenReadyJS("RegiontoRoleActionBTN_XPATH", 20);
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void UpdateRegionToRol() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UpdateRegionToRole_XPATH", 20);
    }

    @Description("Escalation Email")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void EscalationEmail() throws IOException, InterruptedException {
        DosendKeysRandomEmailsWhenReady("RegionEmail_XPATH", 20);
    }

    @Description("Create Region To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void CreateRegionToRole() throws IOException, InterruptedException {
        doclickWhenReady("CreateRegionToRole_XPATH", 20);
    }

    @Description("Assert Create Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreateEscalation() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdateRegiontorole_XPATH", "Successful_TEXT",20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKBTN6_XPATH", 20);
    }
}
