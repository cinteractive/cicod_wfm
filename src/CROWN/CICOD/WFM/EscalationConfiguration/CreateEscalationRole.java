package CROWN.CICOD.WFM.EscalationConfiguration;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class CreateEscalationRole extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("Escalation Main Menu")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void EscalationMainMenu() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationMainMenue_XPATH", 60);
    }

    @Description("Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void EscalationToRole() throws IOException, InterruptedException {
        DoClickWhenReadyJS("EscalationTorole_XPATH", 60);
    }

    @Description("New Escalation To Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewEscalationToRole() throws IOException, InterruptedException {
        Thread.sleep(3000);
        doclickWhenReady("NewEscalationTorole_XPATH", 60);
    }

    @Description("Escalation Role Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void EscalationRoleName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("EscalationRoleName_XPATH", 60);
    }

    @Description("Escalation Role Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void EscalationRoleDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("EscalationRoleDiscription_XPATH", "Decrib_TEXT",60);
    }

    @Description("Create Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void CreateEscalation() throws IOException, InterruptedException {
        doclickWhenReady("CreateEscalationBTN_XPATH",60);
    }

    @Description("Assert Create Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreateEscalation() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AlertMessage_XPATH","Successful_TEXT",20);
    }
}
