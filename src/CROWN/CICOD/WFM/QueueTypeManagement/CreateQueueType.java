package CROWN.CICOD.WFM.QueueTypeManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;
import static CROWN.utility.Utility.DowaitandAcceptAlerwhenReady;

public class CreateQueueType extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Manage_Queue() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Manage_Queue_XPATH", 60);
    }

    @Description("Create Queue Type")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void CreateQueuetype() throws IOException, InterruptedException {
        Thread.sleep(3000);
        DoClickWhenReadyJS("CreateQUEUETYpeBTN_XPATH", 20);
    }

    @Description("Queue Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void QueueName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("QueueTypeName_XPATH", 20);
    }

    @Description("Queue Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void QueueDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("QueueTypeDiscription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Select Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void SelectQueue() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("SelectQueue_XPATH", 5, 20);
    }

    @Description("Create New Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void CreateNewQueue() throws IOException, InterruptedException {
        Thread.sleep(2000);
        doclickWhenReady("CreateQUEUEbtn_XPATH", 20);
        Thread.sleep(3000);
        AcceptAlert();
    }
}