package CROWN.CICOD.WFM.QueueTypeManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class SetUpEscalationForQueueType extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Manage_Queue() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Manage_Queue_XPATH", 60);
    }

    @Description("Queue Type Update Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void QueTypeUpdateAction() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("QueTypeUpdateActionBtn_XPATH", 20);
    }

    @Description("Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Escalation() throws IOException, InterruptedException {
        doclickWhenReady("EscalationBTN_XPATH", 20);
    }

    @Description("View Levels")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void ViewLevels() throws IOException, InterruptedException {
        doclickWhenReady("ViewLevels_XPATH", 20);
    }

    @Description("New Escalation Record")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void NewEscalationRecord() throws IOException, InterruptedException {
        doclickWhenReady("NewEscalationRecord_XPATH", 20);
    }

    @Description("Label")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void Label() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("Label_XPATH", 20);
    }

    @Description("Time Value")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void TimeValue() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("TimeValue_XPATH", 5, 20);
    }

    @Description("Priority")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void Priority() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("EscalationPriority_XPATH", 5, 20);
    }

    @Description("Role")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void Role() throws IOException, InterruptedException {
        doclickWhenReady("Role1_XPATH", 20);
    }

    @Description("Set Up Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void SetUpEscalation() throws IOException, InterruptedException {
        doclickWhenReady("CreateEscalation_XPATH", 20);
    }

    @Description("Assert SetUp Escalation")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 14)
    public void AssertSetUpEscalation() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertEscalation_XPATH", "SetUpEsc_TEXT",20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 15)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("SetUpEOKButton_XPATH", 20);
    }
}
