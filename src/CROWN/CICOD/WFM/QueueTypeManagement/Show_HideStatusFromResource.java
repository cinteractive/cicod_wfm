package CROWN.CICOD.WFM.QueueTypeManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;

public class Show_HideStatusFromResource extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Manage_Queue() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Manage_Queue_XPATH", 60);
    }

    @Description("Queue Type Update Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("QueTypeUpdateActionBtn_XPATH", 20);
    }

    @Description("Link Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void LinkStatus() throws IOException, InterruptedException {
        doclickWhenReady("LinkStatus_XPATH", 20);
    }

    @Description("Add Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void AddStatus() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AsDdStatusBTN_XPATH", 60);
    }

    @Description("WorkOrder Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void WorkOrderStatus() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("WorkOrderStatus_XPATH", 5, 20);
    }

    @Description("Priority")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void Priority() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("WorkOrederPriority_XPATH", 15, 20);
    }

    @Description("Create")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Create() throws IOException, InterruptedException {
        doclickWhenReady("CreateNewStatusa_XPATH", 20);
    }

    @Description("Assert Create Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void AssertCreate() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("Assertsuccessfulstatuscreation_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("ShowHideStatusOKBTN_XPATH", 20);
    }

    @Description("Show To Resource")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void ShowToResource() throws IOException, InterruptedException {
        doclickWhenReady("ShowToResource_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 16)
    public void OK1() throws IOException, InterruptedException {
        Thread.sleep(3000);
        this.OK();
    }

    @Description("Hide From Resource")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 17)
    public void HideFromResource() throws IOException, InterruptedException {
        doclickWhenReady("HideFromResource_XPATH", 20);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("Assert Hide To Resource")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 19)
    public void AssertHideTOResource() throws IOException, InterruptedException {
        CheckElementPresent("AssertHideStatus_XPATH", 20);
    }
}
