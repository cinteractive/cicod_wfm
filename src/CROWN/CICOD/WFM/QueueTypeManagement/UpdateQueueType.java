package CROWN.CICOD.WFM.QueueTypeManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import com.google.api.services.gmail.model.Thread;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;
import static CROWN.utility.Utility.DowaitandAcceptAlerwhenReady;

public class UpdateQueueType extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue Type")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Manage_QueueType() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageQueueTypeBTN_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        DoClickWhenReadyJS("QueTypeUpdateActionBtn_XPATH", 20);
    }

    @Description("Add Queue Type Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void AddQueueTypeStatuse() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AddQueueTypeStatuses_XPATH", 20);
    }

    @Description("Add Queue Type Status Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void AddQueueTypeStatuseName() throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(3);
        DoSelectValuesByIndexRandom("QUENAME_XPATH", 3, 20);
    }

    @Description("Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Name() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("QueuetypeName_XPATH", 20);
    }

    @Description("Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void Description() throws IOException, InterruptedException {
        DoSendKeysWhenReady("DSCC_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Update")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Update() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Save123_XPATH", 20);
        TimeUnit.SECONDS.sleep(5);
        AcceptAlert();
    }

    @Description("Assert Create New Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void AssertCreateNewStatus() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertUpdteQueueT_XPATH", "Successful_TEXT",20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void OK() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UpdateQueueTyOK_XPATH", 20);
    }
}
