package CROWN.CICOD.WFM.QueueTypeManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;

public class Suspend_UnsuspendQueueTypeStatus extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Manage_Queue() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Manage_Queue_XPATH", 60);
    }

    @Description("Queue Type Update Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("QueTypeUpdateActionBtn_XPATH", 20);
    }

    @Description("Link")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Link() throws IOException, InterruptedException {
        doclickWhenReady("LinkStatus1_XPATH", 20);
    }

    @Description("Suspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Suspend() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SuspendQueueTypeStatus_XPATH", 60);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("Assert Suspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void AssertSuspend() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("Assertsuspdep_XPATH", "Suspended_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("QutypeStatusOKBTN_XPATH", 20);
    }

    @Description("Unsuspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void Unsuspend() throws IOException, InterruptedException {
        doclickWhenReady("UnsuspendQueueTypeStatus_XPATH", 20);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void OK2() throws IOException, InterruptedException {
        this.OK();
    }
}
