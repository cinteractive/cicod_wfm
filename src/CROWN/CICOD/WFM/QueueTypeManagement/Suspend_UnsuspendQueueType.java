package CROWN.CICOD.WFM.QueueTypeManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.io.IOException;
import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.AcceptAlert;

public class Suspend_UnsuspendQueueType extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("wfm")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void wfm() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue Type")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Manage_QueueType() throws IOException, InterruptedException {
        DoClickWhenReadyJS("ManageQueueTypeBTN_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("QueTypeUpdateActionBtn_XPATH", 20);
    }

    @Description("Suspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Suspend() throws IOException, InterruptedException {
        DoClickWhenReadyJS("QueueTypeSuspendBTN_XPATH", 60);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("OKQUE_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Action2() throws IOException, InterruptedException {
        this.Action();
    }

    @Description("Unsuspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void Unsuspend() throws IOException, InterruptedException {
        doclickWhenReady("QueueTypeUnsuspendBTN_XPATH", 20);
        Thread.sleep(3000);
        AcceptAlert();
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void OK2() throws IOException, InterruptedException {
        this.OK();
    }
}
