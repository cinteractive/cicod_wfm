package CROWN.CICOD.WFM.QueueTypeManagement;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;

public class AddStatusToQueueType extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void Wfm() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("WorkOrder Manager")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void WorkOrderManager() throws IOException, InterruptedException {
        DoClickWhenReadyJS("WorkOrderManager_XPATH", 60);
    }

    @Description("Manage Queue")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Manage_Queue() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Manage_Queue_XPATH", 60);
    }

    @Description("Queue Type Update Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void QueTypeUpdateAction() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("QueTypeUpdateActionBtn_XPATH", 20);
    }

    @Description("Links")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Links() throws IOException, InterruptedException {
        doclickWhenReady("LinkSt_XPATH", 20);
    }

    @Description("Queue Type Update Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void AddStatus() throws IOException, InterruptedException {
        doclickWhenReady("AsDdStatusBTN_XPATH", 20);
    }

    @Description("Select Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void SelectStatus() throws IOException, InterruptedException {
        DoSelectValuesByIndexRandom("WorkOrderStatus_XPATH", 7, 20);
    }

    @Description("Status Priority")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void StatusPriority() throws IOException, InterruptedException {
        DosendKeysRandomNumberWhenReady("WorkOrederPriority_XPATH", 15000, 20);
    }
    @Description("Create New Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void CreateNewStatus() throws IOException, InterruptedException {
        doclickWhenReady("CreateNewStatusa_XPATH", 20);
    }

    @Description("Assert Create New Status")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void AssertCreateNewStatus() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("aaaaa_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("AddStatusOKbutton_XPATH", 20);
    }
}
