package CROWN.CICOD.WFM.WebForm;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.ActionsClass.*;
import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.DatePicker.DatePickerJE;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;
import static CROWN.utility.ExcelUtil.DosendKeysRandomListwordsWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Utility.DoclickWhenReady;

public class CreateWebform extends TestBase {

    @Description("Login to Cicod")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("Access WFM")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void WorkFlowManager() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Wfm_XPATH", 30);
    }

    @Description("Assert Successful Access of WFM")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssertWorkFlowManager() throws IOException, InterruptedException {
        CheckElementPresent("wfmConfig_XPATH",30);
    }

    @Description("Access Configuration")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void AccessConfiguration() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("wfmConfig_XPATH", 30);
    }

    @Description("Assert Webform and WorkOrder Extension")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AssertWebForm_WorkOrderExtension() throws IOException, InterruptedException {
        CheckElementPresent("webform_XPATH", 30);
    }

    @Description("Access WebForm")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Accesswebform() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("webform_XPATH", 30);
    }

    @Description("Create Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void CreateWebForm() throws IOException, InterruptedException {
        DoClickWhenReadyJS("CreateWebF_XPATH", 30);
    }

    @Description("Create Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void Webform_Name() throws IOException, InterruptedException {
        Thread.sleep(2000);
        DosendKeysRandomListwordsWhenReady("WebFormName_XPATH", 30);
    }

    @Description("Web Form Description")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void Webform_Description() throws IOException, InterruptedException, AWTException {
        DoSendKeysByActionClassFluentWait("WebformDecritpion_XPATH", "Decrib_TEXT", 30);
    }

    @Description("Web Form Queue")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void Webform_Queue() throws IOException, InterruptedException, AWTException {
        DoDoubleClickActionWhenReady("Webformqueue_XPATH", 30);
    }

    @Description("Search Queue")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void SearchQueue() throws IOException, InterruptedException, AWTException {
        DoActionsSendKeys("SearchQue_XPATH", "Ooo_TEXT", 30);
    }

    @Description("Assert Search Queue")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void AssertSearchQueue() throws IOException, InterruptedException, AWTException {
        CheckElementPresent("asorder_XPATH", 20);
    }

    @Description("Select Queue Type")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 14)
    public void SelectQueueType() throws IOException, InterruptedException, AWTException {
        DoclickWhenReady("Orderr_XPATH", "Ooo_TEXT", 30);
        DoclickWhenReady("wfmConfig_XPATH", "Ooo_TEXT", 30);
    }

    @Description("Web Form Queue Type")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 15)
    public void Webform_QueueType() throws IOException, InterruptedException, AWTException {
        DoDoubleClickActionWhenReady("WebQueueT_XPATH", 30);
    }

    @Description("Webform_Queue Type PickUp")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 16)
    public void Webform_QueuetypePickUp() throws IOException, InterruptedException, AWTException {
        DoClickWithMoveToElement("Pickup_XPATH", 30);
        DoclickWhenReady("wfmConfig_XPATH", "Ooo_TEXT", 30);
    }

    @Description("Initila Email Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 17)
    public void InitialEmailResponse() throws IOException, InterruptedException, AWTException {
        DoSendKeysByActionClassFluentWait("InitialEmailRespoms_XPATH", "Decrib_TEXT", 30);
    }

    @Description("Display From for a  Particular Period")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 18)
    public void DisplayFromPeriod() throws IOException, InterruptedException, AWTException {
        DoclickWhenReady("DisplayformPeriod_XPATH", "Ooo_TEXT", 30);
    }

    @Description("StartDate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 19)
    public void StartDate() throws IOException, InterruptedException, AWTException {
        DatePickerJE("WebDatefrom_XPATH", "2020-10-22");
    }

    @Description("EndDate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 20)
    public void EndDate() throws IOException, InterruptedException, AWTException {
        DatePickerJE("WebDateTo_XPATH", "2021-10-22");
    }

    @Description("EndDate")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 21)
    public void AllowAttachedFile() throws IOException, InterruptedException, AWTException {
        DoclickWhenReady("AllowAttachedFi_XPATH", "Ooo_TEXT", 30);
    }

    @Description("Save Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 22)
    public void SaveWebForm() throws IOException, InterruptedException, AWTException {
        DoclickWhenReady("Save_XPATH", "Ooo_TEXT", 30);
    }
}
