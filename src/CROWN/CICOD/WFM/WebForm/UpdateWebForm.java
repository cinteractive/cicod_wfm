package CROWN.CICOD.WFM.WebForm;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Epic;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

import static CROWN.utility.ActionsClass.DoClickActionclassWhenReady;
import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;

@Epic("Update WebForm")
@Story("Test Webform Module Module..")
public class UpdateWebForm extends TestBase {

    @Description("Login to Cicod")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void WorkFlowManager() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Wfm_XPATH", 30);
    }

    @Description("Assert Successful Access of WFM")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssertWorkFlowManager() throws IOException, InterruptedException {
        CheckElementPresent("wfmConfig_XPATH", 30);
    }

    @Description("Access Configuration")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void AccessConfiguration() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("wfmConfig_XPATH", 30);
    }

    @Description("Assert Webform and WorkOrder Extension")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AssertWebForm_WorkOrderExtension() throws IOException, InterruptedException {
        CheckElementPresent("webform_XPATH", 30);
    }

    @Description("Access WebForm")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Accesswebform() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("webform_XPATH", 30);
    }

    @Description("Acton Button")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void ActonButton() throws IOException, InterruptedException {
        Thread.sleep(7000);
        getDriver().findElement(By.name("yt9")).click();
    }

    @Description("Update")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void Update() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("webformUpdate_XPATH", 30);
    }

    @Description("Save Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 22)
    public void SaveWebForm() throws IOException, InterruptedException, AWTException {
        Thread.sleep(5000);
        DoscrolltoViewClickWhenReady("UpdateWebformBtn_XPATH", 30);
    }
}
