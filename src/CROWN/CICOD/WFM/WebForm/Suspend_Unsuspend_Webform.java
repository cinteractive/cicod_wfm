package CROWN.CICOD.WFM.WebForm;

import CROWN.Base.TestBase;
import CROWN.utility.Login;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import java.awt.*;
import java.io.IOException;
import static CROWN.utility.ActionsClass.DoClickActionclassWhenReady;
import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.ExcelUtil.DoscrolltoViewClickWhenReady;

public class Suspend_Unsuspend_Webform extends TestBase {

    @Description("Login")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login.Login();
    }

    @Description("Access WFM")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void WorkFlowManager() throws IOException, InterruptedException {
        DoscrolltoViewClickWhenReady("Wfm_XPATH", 30);
    }

    @Description("Assert Successful Access of WFM")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssertWorkFlowManager() throws IOException, InterruptedException {
        CheckElementPresent("wfmConfig_XPATH", 30);
    }

    @Description("Access Configuration")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void AccessConfiguration() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("wfmConfig_XPATH", 30);
    }

    @Description("Assert Webform and WorkOrder Extension")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AssertWebForm_WorkOrderExtension() throws IOException, InterruptedException {
        CheckElementPresent("webform_XPATH", 30);
    }

    @Description("Access WebForm")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void Accesswebform() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("webform_XPATH", 30);
    }

    @Description("Acton Button")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void ActonButton() throws IOException, InterruptedException {
        Thread.sleep(7000);
        getDriver().findElement(By.name("yt9")).click();

    }

    @Description("Suspend Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 8)
    public void SuspendWebForm() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("SuspendWebform_XPATH", 30);
    }

    @Description("Accept Suspend Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 9)
    public void AcceptSuspension() throws IOException, InterruptedException, AWTException {
        DoClickActionclassWhenReady("susOK_XPATH", 30);
    }

    @Description("Assert Suspend Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 10)
    public void AssertSuspension() throws IOException, InterruptedException, AWTException {
        CheckElementPresent("assertsu_XPATH", 30);
    }

    @Description("Acton Button")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 11)
    public void ActonButton2() throws IOException, InterruptedException {
        Thread.sleep(7000);
        getDriver().findElement(By.name("yt9")).click();
    }

    @Description("Unsuspend Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 12)
    public void UsuspendWebForm() throws IOException, InterruptedException {
        DoClickActionclassWhenReady("Unsuswebform_XPATH", 30);
    }

    @Description("Accept Unsuspend Web Form")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 13)
    public void AcceptUnsuspension() throws IOException, InterruptedException, AWTException {
        DoClickActionclassWhenReady("ok2_XPATH", 30);
    }
}
