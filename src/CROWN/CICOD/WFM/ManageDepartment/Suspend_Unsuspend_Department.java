package CROWN.CICOD.WFM.ManageDepartment;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.AlertPresent;
import static CROWN.utility.ExcelUtil.doclickWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.Login;
import static CROWN.utility.Utility.AcceptAlert;

public class Suspend_Unsuspend_Department extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("User Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void UserManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UserManagement_XPATH", 60);
    }

    @Description("Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Department() throws IOException, InterruptedException {
        DoClickWhenReadyJS("DepartmentBTN_XPATH", 60);
    }

    @Description("Action")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void Action() throws IOException, InterruptedException {
        Thread.sleep(7000);
        doclickWhenReady("ActionDep_XPATH", 60);
    }

    @Description("Suspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void Suspend() throws IOException, InterruptedException {
        DoClickWhenReadyJS("susdep_XPATH", 60);
    }

    @Description("Assert Suspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void AssertSuspend() throws IOException, InterruptedException {
        Thread.sleep(3000);
        AcceptAlert();
        DoAssertContainsWhenReady("Assertsuspdep_XPATH", "Suspended_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void OK() throws IOException, InterruptedException {
        doclickWhenReady("OKUpdp_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void Action2() throws IOException, InterruptedException {
        this.Action();
    }

    @Description("Unsuspend")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void Unsuspend() throws IOException, InterruptedException {
        doclickWhenReady("unsuspenddep_XPATH", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void OK2() throws IOException, InterruptedException {
        Thread.sleep(3000);
        AcceptAlert();
        this.OK();
    }
}