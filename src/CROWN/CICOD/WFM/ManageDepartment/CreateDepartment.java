package CROWN.CICOD.WFM.ManageDepartment;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.DoAssertContainsWhenReady;
import static CROWN.utility.ExcelUtil.*;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.Login;

public class CreateDepartment extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        Login();
    }

    @Description("WFM")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void WFM() throws IOException, InterruptedException {
        DoClickWhenReadyJS("Wfm_XPATH", 60);
    }

    @Description("User Management")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void UserManagement() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UserManagement_XPATH", 60);
    }

    @Description("Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void Department() throws IOException, InterruptedException {
        DoClickWhenReadyJS("DepartmentBTN_XPATH", 60);
    }

    @Description("New Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void NewDepartment() throws IOException, InterruptedException {
        Thread.sleep(3000);
        DoClickWhenReadyJS("NewTeam_XPATH", 60);
    }

    @Description("New Department Name")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void DepartmentName() throws IOException, InterruptedException {
        DosendKeysRandomListwordsWhenReady("TeamName_XPATH", 20);
    }

    @Description("Department Description")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void DepartmentDescription() throws IOException, InterruptedException {
        DoSendKeysWhenReady("TeamDescription_XPATH", "Decrib_TEXT", 20);
    }

    @Description("Create Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Createdepartment() throws IOException, InterruptedException {
        doclickWhenReady("CreateTeamBTN_XPATH", 20);
    }

    @Description("Assert Create Department")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertCreateDepartment() throws IOException, InterruptedException {
        DoAssertContainsWhenReady("AssertCreateDepartment_XPATH", "Successful_TEXT", 20);
    }

    @Description("OK")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 10)
    public void OKBTN() throws IOException, InterruptedException {
        doclickWhenReady("OKCDE_XPATH", 20);
    }
}
